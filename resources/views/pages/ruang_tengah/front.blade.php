@extends('layouts.front')

@section('title')
Ruang Tengah
@endsection

@section('content')
<div class="container">
    <div class="row my-4">
        <div class="col">
            <h4>Ruang tengah is a safe space to discuss ideas, without strings attached.</h4>
            <p class="mt-4 mb-0">Register below to get news of the next Ruang Tengah.</p>
        </div>
    </div>
</div>

<div class="container" id="front">
    <div class="row">
        <div class="col">

        </div>
    </div>
    <div class="row">
        <div class="col-md-8">

            <form method="POST" action="{{ route('ruang_tengah.register') }}">
                @csrf

                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Name</label>

                    <div class="col-md-5">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-md-2 col-form-label text-md-right">{{'E-Mail'}}</label>

                    <div class="col-md-5">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="organization" class="col-md-2 col-form-label text-md-right">Organization</label>

                    <div class="col-md-5">
                        <input id="organization" type="text" class="form-control @error('organization') is-invalid @enderror" name="organization" value="{{ old('organization') }}" required>

                        @error('organization')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>


                <div class="form-group row">
                    <label for="membership" class="col-md-5 col-form-label text-md-right">Register for ThinkPolicy membership?</label>

                    <div class="col-md-3">
                        <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="membership" value="1" checked='checked'>
                    <label class="form-check-label" for="inlineRadio1">Yes</label>
                    </div>
                        <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="membership" value="0">
                    <label class="form-check-label" for="inlineRadio2">No</label>
                    </div>

                        @error('organization')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-2 offset-md-5">
                        <button type="submit" class="btn btn-white">
                            Sign up
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('css')
<style>

body {
    background-image : url("{{ asset('img/bg_rute1.png')}}");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}

body h1,
body h2,
body h3,
body h4,
body h5,
body h6,
body p,
body label {
    color: white;
}

.btn-white {
    background-color: white;
    color: #444;
}

body a {
    color: #ccc;
}


</style>

@endsection

@section('js')
@endsection

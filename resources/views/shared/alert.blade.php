<?php
  // Process the data
  $fdata = session('fdata'); // getting $flash_data
  $fdata = $fdata['alert'];
  $type = isset($fdata['type']) ? $fdata['type'] : 'primary';
  $dismissable = isset($fdata['dismissable']) ? $fdata['dismissable'] : true;
  $heading = isset($fdata['heading']) ? $fdata['heading'] : false;
  $message = $fdata['message'];
  $link = isset($fdata['link']) ? $fdata['link'] : false;
?>

<section class="section">
  <div class="container">
    <div class="row"></div class="col">
    <div class="alert alert-{{ $fdata['type'] }}">
      @if($heading) <h4 class="alert-{{ $fdata['type'] }}"> {{ $heading }} </h4> @endif
        <p class="mb-0 alert-{{ $fdata['type'] }}">{{ $message }}</p>
      @if($link)
        <hr/>
        {{ $link }}
      @endif
    </div>
    </div></div>
  </div>
</section>
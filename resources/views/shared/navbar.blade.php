@guest
    @php
        $navbar = "navbar-dark";
        $home_url = '/';
    @endphp
@else
    @php
        $navbar = "navbar-light";
        $home_url = '/home';
    @endphp
@endauth

<nav class="navbar navbar-expand-md {{ $navbar }} shadow-sm">
    <div class="container">

        <a class="navbar-brand " href="{{ url($home_url) }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>


<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <!-- Left Side Of Navbar -->
    @guest
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link " href="{{ route('ruang_tengah.front') }}">Ruang Tengah</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Bootcamp</a>
        </li>

    </ul>

    @endguest

    @auth
   <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a id="navbar" class="nav-link " href="#" role="button" aria-haspopup="true" aria-expanded="false" v-pre>
                Ruang Tengah<span class="caret"></span>
            </a>
        </li>

        <li class="nav-item">
            <a id="navbar" class="nav-link " href="#" role="button" aria-haspopup="true" aria-expanded="false" v-pre>
                Bootcamp<span class="caret"></span>
            </a>
        </li>

        <!--<li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle " href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                Ruang Tengah<span class="caret"></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item " href="{{ route('page.edit', ['page' => 'ruang_tengah.edit']) }}">Edit
            </a>
        </li>


        <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle " href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                Academy<span class="caret"></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('page.edit', ['page' => 'academy']) }}">Edit
            </a>
        </li>

        <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                Library<span class="caret"></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('page.edit', ['page' => 'library']) }}">Edit
            </a>
        </li>-->
    </ul> 
    @endauth

    <!-- Right Side Of Navbar -->
    <ul class="navbar-nav ml-auto">
        <!-- Authentication Links -->
        @guest
            <li class="nav-item">
                <a class="nav-link " href="{{ route('login') }}">Login</a>
            </li>
            <!-- @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">Signup</a>
                </li>
            @endif -->
        @else
            @if(Auth::user()->id == 1)
            <li class="nav-item">
                <a class="nav-link " href="{{ route('user.index') }}">User List</a>
            </li>
            @endif

            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item " href="#"
                        onclick="event.preventDefault();
                                        document.getElementById('profile-form').submit();">
                        {{ __('Profile') }}
                    </a>
                    <a class="dropdown-item " href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        @endguest
    </ul>

        </div>
    </div>
</nav>
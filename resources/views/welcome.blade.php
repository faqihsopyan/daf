@extends('layouts.front')

@section('title')
Welcome
@endsection

@section('content')
<div class="container-fluid" id="front">
    <div class="row">
        <div class="col-md-8" class="welcome-hero">

        </div>
    </div>
    <div class="row">
        <div class="col text-center">
        <img class="logo-center" src=" {{ asset('img/TPS_logo_light.png') }}"/>

        <h3>We are a network of young professionals<br />
        That promotes a mindset for policy analysis and design<br />
        Based on evidence and empathy</h3>

        <p class="mt-4">Register your email below to get notified of our programs.</p>

        <form method="POST" action="{{ route('user.sign_up_notification') }}">
            @csrf

            <div class="form-group row">

                <div class="col-md-4 offset-md-4">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-2 offset-md-5">
                    <button type="submit" class="btn btn-white">
                        Register
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('js')
@endsection

@section('css')
<style>
.logo-center {
    width:250px;
}

body {
    background-image: url('{{ asset('img/bg_front1.png')}}');
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}

body h1,
body h2,
body h3,
body h4,
body h5,
body h6,
body p {

}

body a {
    color: #ccc;
}

.nav-link {
    color: white;
}

.btn-white {
    background-color: white;
    color: #444;
}


</style>
@endsection
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function() {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('/page', 'PageController')->except(['show']);
    Route::resource('/ruang_tengah', 'RuangTengahController')->except(['show']);
    Route::resource('/user', 'UserController');
});

Route::get('/ruang_tengah', 'RuangTengahController@front')->name('ruang_tengah.front');
Route::post('/ruang_tengah/register', 'RuangTengahController@register')->name('ruang_tengah.register');

Route::post('/user/get_notified', 'UserController@SignUpNotification')->name('user.sign_up_notification');

Route::get('/{page}', 'PageController@show')->name('page.show');
## Website ThinkPolicy

Website [ThinkPolicy Society](https://thinkpolicy.id).

## Getting Started

Requirements:
1. PHP 7.3+
2. MySQL 8.0+ / MariaDB 10.3.15+
3. Composer 1.9.0+

Optional tools:
1. HeidiSQL [HeidiSQL](https://www.heidisql.com/)

Steps:
1. Clone project
2. Switch to root folder
3. Install PHP packages (`composer install`)
4. Install NPM packages (`npm install`)
5. Build CSS & JS bundles (`npm run dev`)
6. Setup dev environment config (esp. DB settings) in `.env` file
6. Run migration + seed (`php artisan migrate:fresh --seed`)
5. Serve website (`php artisan serve`)
6. View site at `127.0.0.1:8000`
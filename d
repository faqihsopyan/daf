#!/bin/bash

## App options ##
APP_NAME=thinkpolicy.id

## local options ##
LOCAL_IP=192.168.20.1
IMG_NAME=aerlaut/$APP_NAME:latest

DEV_PATH=docker/dev.$APP_NAME
DC_DEV=$DEV_PATH/docker-compose.yml

PROD_PATH=docker/$APP_NAME
DC_PROD=$PROD_PATH/docker-compose.yml
DF_PROD=$PROD_PATH/Dockerfile

## Container locations ##
PHP_PATH=/usr/bin/php
MYSQL_PATH=/usr/bin/mysql
SH_PATH=/bin/sh
ARTISAN_PATH=/var/www/html/artisan

## DB OPTIONS ##
DB_USER=tpsociety
DB_DATABASE=tpsociety


## Usage ##
if [ "$1" == 'help' ] || [ "$1" == '-h' ] || [ "$1" == '--help' ]; then

  echo "
  Script to help run docker
  usage : ./docker [ dev | prod ] command [ .. params ]

  List of commands : 
  up                    - Run the containers
  down                  - Stop and destroy containers
  sh [container name]   - Attach and open up a container into containers
  tinker                - Open up tinker console
  migrate               - Run migrations
  migrate-fresh         - Empty database, migrate
  migrate-fresh-seed    - Empty database, migrate and seed
  db-seed               - Seed only
  "

## Dev enviroment ##
elif [ "$1" == "dev" ]; then

  if [ -n "$2" ]; then

    ## Dev commands ##
    if [ "$2" == "up" ]; then

      sudo ifconfig lo0 alias $LOCAL_IP up
      #docker-sync start
      docker-compose -f $DC_DEV up -d
      echo "Container running at $LOCAL_IP"

    elif [ "$2" == "monitor" ]; then

      sudo ifconfig lo0 alias $LOCAL_IP up
      #docker-sync start
      docker-compose -f $DC_DEV up
      echo "Container running at $LOCAL_IP"

    elif [ "$2" == "down" ]; then

      docker-compose -f $DC_DEV down
      #docker-sync stop
      sudo ifconfig lo0 delete $LOCAL_IP
      echo "IP : $LOCAL_IP removed. Container stopped"

    elif [ "$2" == 'sh' ]; then

      docker-compose -f $DC_DEV exec "$3" $SH_PATH

    elif [ "$2" == 'tinker' ]; then

      docker-compose -f $DC_DEV exec app $PHP_PATH $ARTISAN_PATH tinker

    elif [ "$2" == 'migrate-fresh-seed' ]; then

      docker-compose -f $DC_DEV exec app $PHP_PATH $ARTISAN_PATH migrate:fresh --seed

    elif [ "$2" == 'db-seed' ]; then

      docker-compose -f $DC_DEV exec app $PHP_PATH $ARTISAN_PATH db:seed

    elif [ "$2" == 'mysql' ]; then

      docker-compose -f $DC_DEV exec database $MYSQL_PATH -u $DB_USER -p $DB_DATABASE 
      # docker-compose -f $DC_DEV exec database /bin/sh

    fi

  else

    echo "Usage : ./dockerup [prod|dev] command [ ... param ]"

  fi




## Production enviroment ##
elif [ "$1" == 'prod' ]; then

  if [ -n "$2" ]; then

    ## Production commands ##
    if [ "$2" == "up" ]; then

      docker-compose -f $DC_PROD up -d
      docker-compose -f $DC_PROD exec app $PHP_PATH $ARTISAN_PATH storage:link
      echo "Container running"

    elif [ "$2" == "monitor" ]; then

      docker-compose -f $DC_PROD up
      echo "Container running"

    elif [ "$2" == "down" ]; then

      docker-compose -f $DC_PROD down
      echo "Container stopped"

    elif [ "$2" == "build" ]; then

      # update composer
      composer install

      # update npm
      npm install

      docker rmi -f "$IMG_NAME"
      docker build . -f $DF_PROD -t "$IMG_NAME"

      echo "Image $IMG_NAME built"

    elif [ "$2" == 'sh' ]; then

      docker-compose -f $DC_PROD exec "$3" $SH_PATH

    elif [ "$2" == 'tinker' ]; then

      docker-compose -f $DC_PROD exec app $PHP_PATH $ARTISAN_PATH tinker

    elif [ "$2" == 'migrate-fresh' ]; then

      docker-compose -f $DC_PROD exec app $PHP_PATH $ARTISAN_PATH migrate

    elif [ "$2" == 'migrate-fresh' ]; then

      docker-compose -f $DC_PROD exec app $PHP_PATH $ARTISAN_PATH migrate:fresh

    elif [ "$2" == 'migrate-fresh-seed' ]; then

      docker-compose -f $DC_PROD exec app $PHP_PATH $ARTISAN_PATH migrate:fresh --seed

    elif [ "$2" == 'mysql' ]; then

      docker-compose -f $DC_PROD exec database $MYSQL_PATH -u $DB_USER -p $DB_DATABASE 

    fi

  else

    echo "Usage : ./dockerup [prod|dev] command [ ... param ]"

  fi



# Other options

elif [ "$1" == 'rm' ] && [ "$2" == 'volumes' ]; then

  echo "Deleting volumes... "
  docker volume rm $(docker volume ls --format "{{.Name}}")
  echo "Volumes deleted"

else

  echo "Usage : ./dockerup [prod|dev] command [ ... param ]"

fi
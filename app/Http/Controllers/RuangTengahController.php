<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class RuangTengahController extends Controller
{
    /**
     * Show Ruang Tengah Front Page.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function front()
    {
        return view('pages.ruang_tengah.front');
    }

    /**
     * Register ruang tengah participants.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {

        $request->validate([

            'name' => 'required',
            'email' => 'required|email',
            'organization' => 'required'
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'organization' => $request->organization,
            'membership' => $request->membership,
            'password' => bcrypt('tipolmember')
        ]);

        $flash_data = array(
            'alert' => array(
              'type' => 'success',
              'message' => 'Thank you for registering. We will notify you future Ruang Tengah sessions.'
            )
          );

        return redirect()->route('ruang_tengah.front')->with('fdata', $flash_data);
    }
}
